function countLetter(letter, sentence) {
    let result = 0;

    let checkIfSingleCharacter = letter.length === 1;

    if (checkIfSingleCharacter) {
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i] === letter) {
                result++;
            }
        }
        return result;
    }

    return undefined;
    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.
    // If letter is invalid, return undefined.
}

function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.
    // The function should disregard text casing before doing anything else.
    let disregardCasing = text.toLowerCase();
    let lettersSeen = {};
    // If the function finds a repeating letter, return false. Otherwise, return true.
    for (let i = 0; i < disregardCasing.length; i++) {
        let letter = disregardCasing[i];
        if (lettersSeen[letter]) {
            return false;
        }
        lettersSeen[letter] = true;
    }

    return true;
}

function purchase(age, price) {
    if (age < 13) {
        return undefined;
    } else if ((age >= 13 && age <= 21) || age >= 65) {
        let discount = price * 0.2;
        let finalPrice = price - discount;
        return finalPrice.toFixed(2).toString();
    } else if (age >= 22 && age <= 64) {
        return price.toFixed(2).toString();
    }

    // Return undefined for people aged below 13.
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    // Return the rounded off price for people aged 22 to 64.
    // The returned value should be a string.
}

function findHotCategories(items) {
    const hotCategories = [];

    items.forEach((item) => {
        if (item.stocks === 0 && !hotCategories.includes(item.category)) {
            hotCategories.push(item.category);
        }
    });

    return hotCategories;

    // Find categories that has no more stocks.
    // The hot categories must be unique; no repeating categories.
    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }
    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
}

function findFlyingVoters(candidateA, candidateB) {
    const flyingVoters = candidateA.filter((voter) => candidateB.includes(voter));

    return flyingVoters;
    // Find voters who voted for both candidate A and candidate B.
    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']
    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters,
};
